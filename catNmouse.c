///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  Kai Matsusaka <kairem@hawaii.edu>
/// @date    1/26/22
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>
#define DEFAULT_MAX_NUMBER 2048

int main( int argc, char* argv[] ) {
   printf( "Cat `n Mouse\n" );
   printf( "The number of arguments is: %d\n", argc );

   // This is an example of getting a number from a user
   // Note:  Don't enter unexpected values like 'blob' or 1.5.  
   
   int max = atoi(argv[1]);
   int guess = 0;

   if(max < 1) {printf("Error. Max must be >=1.\n"); exit(1);} //Error Case max


   int randNum = (rand() % max) + 1; //generate a random number


   do {
      printf("OK cat, I’m thinking of a number from 1 to %d.", max);
      printf(" Take a guess: ");
      scanf( "%d", &guess);

      if (guess < 1) {
         printf("Guess must be greater than 1.\n");
         continue;
      }
   
      if (guess > max) {
         printf("Guess must be less than max.\n");
         continue;
      }
   
      if (guess < randNum) {
         printf("No cat... the number I’m thinking of is larger than %d.\n", guess);
         continue;
      }

      if (guess > randNum) {
         printf("No cat... the number I’m thinking of is smaller than %d.\n", guess);
         continue;
      }
   }
   while(guess != randNum);

   printf("You got me.\n");
   printf("|\\---/|\n| o_o |\n \\_^_/\n");

   return 1;  // This is an example of how to return a 1
}

